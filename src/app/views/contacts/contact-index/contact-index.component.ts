import { Component, OnInit } from '@angular/core';
import { Contact } from '@app/core/models';
@Component({
  selector: 'app-contact-index',
  templateUrl: './contact-index.component.html',
  styleUrls: ['./contact-index.component.scss']
})
export class ContactIndexComponent implements OnInit {
  public contacts: Contact[] = [{id:1,email:'vickybees@gmail.com',name:'Vignesh',phone:'123456'}];
  constructor() { }

  ngOnInit(): void {
  }

}
